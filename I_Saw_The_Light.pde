/**
 * I Saw The Light.
 * 
 * I wanted to make something with colors.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * 2019.06.09
 */

void setup() {
  size(1080, 1080);
  colorMode(HSB, 360, 100, 100, 100);
  smooth();
  noLoop();
}

void draw() {

  // color variation within 60.0 degrees
  float hueDiv  = 60.0;
  float hueInit = random(hueDiv);

  translate(width * 0.5, height * 0.5);

  // draw three images with differect color
  for (int imgCnt = 1; imgCnt <= 3; imgCnt++) {

    float hueBase = 320 + (hueInit + imgCnt * 20.0) % hueDiv; // choose nice color
    float factorA = random(1.0, 2.0);
    float factorB = random(1.0, 2.0);

    background((hueBase + 180.0) % 360.0, 100.0, 100.0, 100.0);  

    blendMode(DIFFERENCE);
    drawWave(400, factorA, factorB, hueBase);
    blendMode(ADD);
    drawWave(100, factorA, factorB, hueBase + 30.0);
  
    casing();
    saveFrame("frames/" + String.format("%04d", imgCnt) + ".png");

  }
  
  exit ();

}

/**
 * drawWave
 * @param  _lineMax  : number of waves to draw.
 * @param  _hueBase  : wave color.
 * @param  _factorA, _factorB : wave shape factor.
 */
private void drawWave(int _lineMax, float _factorA, float _factorB, float _hueBase) {

    noStroke();
    fill(_hueBase % 360.0, 80, 30.0, 40.0);

    pushMatrix();
    for (int lineCnt = 0; lineCnt < _lineMax; ++lineCnt) {

      float lineRatio = map(lineCnt, 0, _lineMax, 0.0, 1.0);

      rotate(PI * lineRatio);
      beginShape();
      for (int idxX = 0; idxX <= width; ++idxX) {

        float xRatio = map(idxX, 0, width, 0.0, 1.0);

        float coordX = width * 1.3 * (xRatio - 0.5);
        float coordY = height * 0.1 * customNoise((_factorA + xRatio) * TWO_PI, (_factorB + xRatio) * TWO_PI);

        // I can't use rotate() because of vertex()
        float radian = TWO_PI * lineRatio;
        float pointX = coordX * cos(radian) - coordY * sin(radian);
        float pointY = coordX * sin(radian) + coordY * cos(radian);

        vertex(pointX, pointY);

      }
      endShape();
    }
    popMatrix();

}
  
/**
 * customNoise : returns -1.0 .. 1.0 almost random but interesting value
 */
private float customNoise(float _x, float _y) {
  return pow(sin(_x), 3) * cos(pow(_y, 2));
}

/**
 * casing : draw fancy casing
 */
private void casing() {
  rectMode(CENTER);
  blendMode(BLEND);
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(46.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
}
